package com.plexus.main.kafkahttpdemo;


import com.plexus.main.kafkahttpdemo.model.Order;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class ServiceConsumer {

        private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

       @KafkaListener(groupId = "${spring.kafka.consumer.group-id}", topics = "${org.apache.kafka.common.internal.topic}", containerFactory = "kafkaListenerContainerFactory" )

    public void messageRead(Order value) {
        System.out.println("JSON Message Successfully Read from the topic" + ":" + value.toString());
//           String dataread = message.toString();
//        logger.info("JSON Message Successfully Read from the topic" + ":" + dataread);
    }
}
