package com.plexus.main.kafkahttpdemo;

import com.plexus.main.kafkahttpdemo.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("order")
public class EventController {

    @Autowired
    ServiceProducer serviceProducer;

    @PostMapping("/publish/{message}")
    public String send(@PathVariable String message) {
        serviceProducer.sendMessage(message);
        return ("Message Published Successfully to topic");
    }

    @PostMapping("/publish")
    public String send(@RequestBody Order order) {
        serviceProducer.sendMessage((order));
        return ("Message Published Successfully to topic");
    }
}

